# Microservices Activities for LibreFoodPantry Onboarding

**NOTE: These activities are under active development. Some are old versions in need of updating, some are empty placeholders. The activities that comprise the [primary path](#suggested-sequence-of-activities) are being developed for use during the Fall 2024 semester (September-December 2024). They should be (mostly) usable by the end of December 2024.**

Provides materials that can be used as in-class activities or onboarding
tutorials for students or independent developers learning to work within the
microservices architecture that is being used by
[LibreFoodPantry](https://librefoodpantry.org).

LibreFoodPanty has adopted the microservices architecture because it makes it easier for classes, student teams, or individual developers to

* work on smaller, easier to understand modules
* develop code in whatever language or with whatever tools they are already comfortable
* redesign/redevelop modules to meet the needs of their own campus’ food pantry
* work on frontend or backend code based on interest or course goals

But most students and faculty are unfamiliar with the microservices
architecture, and so learning materials are needed to get them up-to-speed
on this style of development.

The set of [POGIL](https://pogil.org) activities teach students to

* design a REST API
* implement the REST API as a backend server with a persistence layer using a document database
* develop and implement a frontend that interacts with the user and calls the backend through the REST API

The activities are developed around a version of the
[Thea's Pantry GuestInfoSystem](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem).
[Theas Pantry](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry)
is a member project of LibreFoodPantry. These activities use the
MicroServicesKit - a GitLab group consisting of

* code repositories, each "captured" at a particular commit
* documentation and issue repositories, each "captured" at a particular commit
* this activities repository, "captured" at a particular commit
* an instructor guide

To use these activities, follow the instructions in the
[Deploy the Microservices Kit section of this README file](#deploy-the-microservices-kit)
to create one or more GitLab groups for you, your students, or your team(s)
to use while working through the activities.

The working code given to explore, modify, and extend is containerized in the
same way that LFP services are containerized in Docker.

## Suggested sequence of activities

```plantuml
rectangle "LFP01 Introduction to the Domain: HFOSS, LibreFoodPantry, and Thea's Pantry GuestInfoSystem" as LFP01
rectangle "MS01 Software Architectures from Monolith to Microservices" as MS01
rectangle "MS02 Introduction to REST API Calls" as MS02
rectangle "MS03 Exploring REST API Calls" as MS03
rectangle "MS04 Specifying REST API Calls" as MS04
rectangle "MS05 Exploring REST API Implementation" as MS05
rectangle "MS06 Implementing New REST API Calls" as MS06
rectangle "MS07 Exploring the Implementation of a Simplified REST API Front End\n(In Progress)" as MS07
rectangle "MS08 Designing Components for a Front End\n(Planned)" as MS08
rectangle "MS09 Implementing New Functionality for a Front End\n(Planned)" as MS09
rectangle "DR01 Understanding `docker-compose.yaml`` in Backend" as DR01
rectangle "DR02 Understanding Dockerfile in Backend" as DR02
rectangle "MDB01 Exploring the Persistence Layer using a Document Database" as MDB01
rectangle "MDB02 Exploring the Implementation of Persistence using a Document Database" as MDB02
rectangle "SV01 SemVer - Semantic Versioning" as SV01

LFP01-->MS01
MS01-->MS02
MS02-->MS03
MS03-->MS04
MS04-->MS05
MS05-->MS06
MS06-->MS07
MS07-->MS08
MS08-->MS09

MS03-[dotted]->SV01:Optional
SV01-[dotted]->MS04

MS05-[dotted]->MDB01:Optional (Future?)
MDB01-[dotted]->MDB02:Optional (Future?)
MDB01-[dotted]->MS06
MDB02-[dotted]->MS06

MS05-[dotted]->DR01:Optional
DR01-[dotted]->MS06
MS05-[dotted]->DR02:Optional
DR02-[dotted]->MS06
```

## Deploy the Microservices Kit

* Each individual or team that will work on the Microservices Activities will
  need to have it's own copy of the Microservices Kit deployed.
* Each copy of the Microservices Kit will be deployed to a GitLab subgroup
  that contains subgroups and projects for the Thea's Pantry code and
  documentation.

To deploy a copy of the Microservices Kit, click the `Open in Gitpod` button
below. (You will need to do this once per deploy.)

Requirements:

* A GitLab account
* A Gitpod account

Notes:

* Before running the deployer, you will want to have created the GitLab
  group or subgroup where you wish to deploy each kit.
* You will need to create a GitLab personal access token. Instructions are
  given by the deployer. We suggest:
  * set the personal access token to expire as soon as you will be done
    with your deployment(s) (you can set an expiration as soon as the next
    day)
  * copy the personal access token to a temporary file on your computer -
    if you are doing multiple deploys, you will be able to use the same
    token for all of them.
* All deployments are made to a subgroup named `microservices-theaspantry`.
    If you are doing multiple deploys, you will want to put each in it's own
    GitLab group or subgroup (previously created).
* The deployer does not currently make copies of the Microservices
  Activities for each deployment. You will need to make a copy manually by
  forking the Microservices Activities. This will be done automatically in
  the future.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/?autostart=true&useLatest=true&editor=xterm#script_url=https%3A%2F%2Fgitlab.com%2FLibreFoodPantry%2Ftraining%2Fmicroservices%2Fmicroserviceskit%2Fmicroserviceskit-deployer-gitpod%2F-%2Fraw%2Fmain%2Fdeploy.yaml%3Fref_type%3Dheads/https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit-deployer-gitpod)

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
