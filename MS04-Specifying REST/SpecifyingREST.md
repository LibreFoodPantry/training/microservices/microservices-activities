# Specifying REST API Calls

## Content Learning Objectives

After completing this activity, students should be able to:

*

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

For this activity, you should have a *Technician* role. The Technician will be the one to have Visual Studio Code running, and the repositories open in Dev Containers. You may want to make sure your Technician is a team member with a more powerful computer.

You can combine the Technician role with another role if you do not have enough team members.

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Technician |
Presenter |
Recorder |
Reflector |

## Prerequisites

You should have a [MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit) GitLab subgroup that has been created for you (either by your instructor, your team leader, or yourself). It will contain a number of respositories. (These activities are likely in a Microservices Activities repository in this subgroup.)

If not, you should follow the instructions in the [README for the MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit/-/blob/main/README.md) to create one.

You need to have the GuestInfoBackend repository cloned to your computer to work on this activity.

## OpenAPI Specification

> The OpenAPI Specification (OAS) defines a standard, programming language-agnostic interface description for REST APIs, which allows both humans and computers to discover and understand the capabilities of a service without requiring access to source code, additional documentation, or inspection of network traffic. When properly defined via OpenAPI, a consumer can understand and interact with the remote service with a minimal amount of implementation logic. Similar to what interface descriptions have done for lower-level programming, the OpenAPI Specification removes guesswork in calling a service.
>
> &mdash; [OpenAPI Specification V3.0.3](http://spec.openapis.org/oas/v3.0.3)

The API for the GuestInfoSystem that you have been exploring was specified using the OpenAPI specification.

## Model 1 - `specification/openapi.yaml`

### Open the GuestInfoBackend Repository in Visual Studio Code

1. Follow the instructions in the GuestInfoBackend project's README.md to open
the project either locally or in Gitpod.
2. Open the GuestInfoSystem specification file at `specification/openapi.yaml`.
3. Open the `specification/openapi.yaml` file using the Swagger Preview` extension
   1. Open the `Command Palette` (`F1` or right-click or `Ctrl+Shift+P`).
   2. Open with `Preview Swagger`.
   3. Arrange the `specification/openapi.yaml` file and the `Swagger Preview` windows to be side by side, so that you can see both as you answer the questions.

### `openapi.yml`

* This is a [YAML](https://en.wikipedia.org/wiki/YAML) file. YAML is a superset of JSON.
* It can be translated by the `Swagger Preview` extension for Visual Studio Code into the HTML files with the API's documentation that you explored in Activity MS03.
* It can be previewed in the browser in GitLab as well.
* It can be used by [express-openapi-validator](https://github.com/cdimascio/express-openapi-validator) by an [ExpressJS](https://expressjs.com/) backend server to validate api calls (paths, parameters, request bodies, and response bodies) and automatically generate error responses for the developer. (We will look at the ExpressJS backend in Activity MS05.
* There are [many other tools](https://openapi.tools/) that can process OpenAPI specifications.

### Questions - Model 1

1. `parameters:` Compare `/guests` to `/guests/{wsuID}`.
    1. Why does `/guests/{wsuID}` have a parameters section, while `/guests` does not?
    2. How is that specified in the `specification/openapi.yaml` file?
    3. Look at the `in:` label in the paths. Why does it specify `path`? How does that relate to the format of the REST API call that you made in MS03?
    4. What is the value of the `required:` label? Do you think this could ever be `false` for a `path` parameter?
    5. What is the `schema:` used for?  
    6. What is `$ref` used for? Why is this a good idea?
    7. Does `parameters:` in `/guests/{wsuID}` apply to all HTTP methods? How can you tell?
2. `requestBody:` Compare `/guests` to `/guests/{wsuID}`.
    1. Why does the `requestBody:` not appear in `/guests`?
    2. Which HTTP method(s) does the `requestBody:` appear under in `/guests/{wsuID}? Explain why.
3. `responses:` Compare `/guests` to `/guests/{wsuID}`.
    1. Which HTTP method(s) does `responses:` appear under? Is there any HTTP method that does not have a `responses:` section? Explain why.
4. Does `operationID` appear in the documentation (Swagger Preview)? Can you guess what it is used for?
5. Look at the rest of the file, comparing it to the Swagger Preview, and note any other interesting things you find.

## Model 2 - Developer Tools

When available (and well-documented and stable) we should use third-party tools that make our lives easier as developers.

For working with our OpenAPI specifications, we have decided to use the
[Spectral](https://stoplight.io/open-source/spectral) API style-guide enforcer and linter.

Spectral can be used to lint an OpenAPI file from the command line, and has a Visual
Studio Code extension to check the YAML file as you type:

Rather than installing [Node.js](https://nodejs.org/) and Spectral, we have decided to use a Docker image that has both installed, and then provide some shell scripts to make it easier to run the commands. These shell scripts are in the `commands` directory.

### Questions - Model 2

1. Open `specification/openapi.yaml`.
2. Open a Terminal and run `bin/spectral.sh`. Note the result:
3. Change `paths:` to `path:` and save your change.
4. What happens in the VSCode editor?
5. Hover over the red squigglies. Note the message:
6. Run `bin/spectral.sh` again and note the result:
7. Undo your change and save the file.

This command runs the Spectral CLI `lint` command.

There are other linters that you can run when you make changes to project. Running `bin/lint.sh` will run all the linters for this project (not just for the API). You should run these linters before you commit and push your changes, so that you cannot merge syntactically incorrect files.

## Additional OpenAPI Resources

* [OpenAPI Map](https://openapi-map.apihandyman.io/) at [API Handyman](https://apihandyman.io/)

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
